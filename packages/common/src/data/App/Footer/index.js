const data = {
  menuWidget: [
    {
      id: 1,
      title: 'About Us',
      menuItems: [
        {
          id: 1,
          url: '#',
          text: 'Support Center'
        },
        {
          id: 2,
          url: '#',
          text: 'Customer Support'
        },
        {
          id: 3,
          url: '#',
          text: 'About Us'
        },
        {
          id: 4,
          url: '#',
          text: 'Copyright'
        },
        {
          id: 5,
          url: '#',
          text: 'Popular Campaign'
        }
      ]
    },
    {
      id: 2,
      title: 'Our Information',
      menuItems: [
        {
          id: 1,
          url: '#',
          text: 'Return Policy'
        },
        {
          id: 2,
          url: '#',
          text: 'Privacy Policy'
        },
        {
          id: 3,
          url: '#',
          text: 'Terms & Conditions'
        },
        {
          id: 4,
          url: '#',
          text: 'Site Map'
        },
        {
          id: 5,
          url: '#',
          text: 'Store Hours'
        }
      ]
    }
  ]
};
export default data;
