import Icon1 from '../../../assets/image/app/1.svg';
import Icon2 from '../../../assets/image/app/2.svg';
import Icon3 from '../../../assets/image/app/3.svg';
import Icon4 from '../../../assets/image/app/4.svg';
import Icon5 from '../../../assets/image/app/5.svg';
import Icon6 from '../../../assets/image/app/6.svg';

const data = {
  features: [
    {
      id: 1,
      image: Icon2,
      title: 'Forum'
    },
    {
      id: 2,
      image: Icon3,
      title: 'News Letter'
    },
    {
      id: 3,
      image: Icon4,
      title: 'Realtime Notification'
    },

    {
      id: 4,
      image: Icon5,
      title: 'Mailing Information System'
    },

    {
      id: 5,
      image: Icon6,
      title: 'Online Assesments'
    },

    {
      id: 6,
      image: Icon1,
      title: 'Data Folder'
    }
  ]
};
export default data;
