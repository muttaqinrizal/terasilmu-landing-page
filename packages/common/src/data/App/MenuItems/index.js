const data = {
  menuItems: [
    {
      label: 'Layanan Kami',
      path: '#services',
      offset: '100'
    },
    {
      label: 'Control Remotely',
      path: '#control',
      offset: '100'
    },
    {
      label: 'Kenapa Memilih Kami',
      path: '#keyfeature',
      offset: '0'
    },
    {
      label: 'Akses Kapanpun di Manapun',
      path: '#partners',
      offset: '-100'
    },
    {
      label: 'Partner Kami',
      path: '#payments',
      offset: '100'
    },
    {
      label: 'Testimonial',
      path: '#testimonialSection',
      offset: '100'
    }
  ]
};
export default data;
