import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import PropTypes from 'prop-types';
import Box from 'common/src/components/Box';
import Text from 'common/src/components/Text';
import Heading from 'common/src/components/Heading';
import FeatureBlock from 'common/src/components/FeatureBlock';
import Container from 'common/src/components/UI/Container';
import FeatureSectionWrapper from './featureSection.style';
import {
  EmailInputWrapper,
  NameInputWrapper,
  MessageInputWrapper
} from '../Banner/banner.style';
import Input from 'common/src/components/Input';
import Button from 'common/src/components/Button';
import { ButtonWrapper } from 'common/src/components/FeatureBlock/featureBlock.style';

const ContactSection = ({
  row,
  col,
  sectionHeader,
  sectionTitle,
  sectionSubTitle,
  featureTitle,
  featureDescription,
  iconStyle,
  contentStyle,
  blockWrapperStyle,
  button
}) => {
  const Data = useStaticQuery(graphql`
    query {
      appJson {
        features {
          id
          title
          description
          icon
        }
      }
    }
  `);

  return (
    <FeatureSectionWrapper id="services">
      <Container>
        <Box {...sectionHeader}>
          <Text {...sectionSubTitle} />
          <Heading {...sectionTitle} />
        </Box>
        <div style={{ display: 'flex' }}>
          <div style={{ flex: 1 }}>
            <Text content="Nama Lengkap" />
            <NameInputWrapper>
              <Input
                inputType="text"
                placeholder="Nama Lengkap"
                iconPosition="left"
                aria-label="email"
              />
            </NameInputWrapper>
          </div>
          <div style={{ flex: 1 }}>
            <Text content="Email" />
            <EmailInputWrapper>
              <Input
                inputType="email"
                placeholder="contoh@gmail.com"
                iconPosition="left"
                aria-label="email"
              />
            </EmailInputWrapper>
          </div>
        </div>
        <div>
          <Text content="Pesan" />
          <MessageInputWrapper>
            <Input
              inputType="textarea"
              placeholder="Pesan"
              iconPosition="left"
              aria-label="email"
            />
          </MessageInputWrapper>
        </div>
        <div
          style={{ display: 'flex', justifyContent: 'center', marginTop: 30 }}
        >
          <ButtonWrapper>
            <Button
              title="KIRIM PESAN"
              {...button}
              style={{ backgroundColor: '#00CEAB' }}
            />
          </ButtonWrapper>
        </div>
      </Container>
    </FeatureSectionWrapper>
  );
};

// FeatureSection style props
ContactSection.propTypes = {
  sectionHeader: PropTypes.object,
  row: PropTypes.object,
  col: PropTypes.object,
  sectionTitle: PropTypes.object,
  sectionSubTitle: PropTypes.object,
  featureTitle: PropTypes.object,
  featureDescription: PropTypes.object
};

// FeatureSection default style
ContactSection.defaultProps = {
  // section header default style
  sectionHeader: {
    mb: ['30px', '30px', '30px', '56px']
  },
  // sub section default style
  sectionSubTitle: {
    content: 'TERTARIK MENGGUNAKAN',
    as: 'span',
    display: 'block',
    fontSize: '14px',
    letterSpacing: '0.13em',
    fontWeight: '700',
    color: '#00CEAB',
    mb: '10px',
    textAlign: ['center']
  },
  // section title default style
  sectionTitle: {
    content: 'Hubungi Kami',
    fontSize: ['20px', '24px', '24px', '24px', '30px'],
    fontWeight: '400',
    color: '#0f2137',
    letterSpacing: '-0.025em',
    mb: '0',
    textAlign: ['center']
  },
  // feature row default style
  row: {
    flexBox: true,
    flexWrap: 'wrap'
  },
  // feature col default style
  col: {
    width: [1, 1 / 2, 1 / 3, 1 / 3, 1 / 3]
  },
  // feature block wrapper default style
  blockWrapperStyle: {
    p: ['20px', '20px', '20px', '40px']
  },
  // feature icon default style
  iconStyle: {
    width: '84px',
    height: '84px',
    m: '0 auto',
    borderRadius: '50%',
    bg: '#fff',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: '36px',
    color: '#29cf8a',
    overflow: 'hidden',
    mb: ['20px', '20px', '20px', '30px'],
    border: '1px solid rgba(36, 74, 117,0.1)'
  },
  // feature content default style
  contentStyle: {
    textAlign: 'center'
  },
  // feature title default style
  featureTitle: {
    fontSize: ['18px', '20px'],
    fontWeight: '400',
    color: '#0f2137',
    lineHeight: '1.5',
    mb: ['10px', '10px', '10px', '20px'],
    letterSpacing: '-0.020em'
  },
  // feature description default style
  featureDescription: {
    fontSize: ['14px', '15px'],
    lineHeight: '1.75',
    color: '#343d48'
  }
};

export default ContactSection;
